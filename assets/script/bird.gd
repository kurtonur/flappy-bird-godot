extends KinematicBody2D

const UP= Vector2(0,-1)
const GRAVITY = 25
const ACCELERATION = 30
const MAX_SPEED = 175
const JUMP_HIGH = -350

var motion = Vector2()
var spriteColor = 1
func _ready():
	randomize()
	spriteColor = randi()%3+1
	$AnimatedSprite.play("fly"+String(spriteColor))
	$headDown.interpolate_property($AnimatedSprite,"rotation_degrees",$AnimatedSprite.rotation_degrees,85,1.0,Tween.TRANS_QUAD,Tween.EASE_IN_OUT)
	pass
	
func _physics_process(delta):
	if globe.gameState == globe.GAMESTATE.play:
		gameOver()
		motion.y +=GRAVITY
		motion.x = min(motion.x+ACCELERATION,MAX_SPEED)
		motion=move_and_slide(motion,UP)
	elif globe.gameState == globe.GAMESTATE.death:
		motion=move_and_slide(Vector2(0,600),UP)
	pass
	
func _input(event):
	if event is InputEventScreenTouch && event.pressed:
		jump()
	pass

func jump():
	if globe.gameState == globe.GAMESTATE.menu:
		globe.gameState=globe.GAMESTATE.play
	if globe.gameState == globe.GAMESTATE.play:
		if !is_on_floor():
			if $headDown.is_active():
				$headDown.stop_all()
		$AnimatedSprite.rotation_degrees= -25
		$headDown.interpolate_property($AnimatedSprite,"rotation_degrees",$AnimatedSprite.rotation_degrees,85,1.0,Tween.TRANS_CIRC,Tween.EASE_IN_OUT)
		$headDown.start()
		motion.y=JUMP_HIGH
		$audio/jump.play()
	pass
	
func gameOver():
	if is_on_wall() or is_on_floor() or is_on_ceiling():
		globe.gameState=globe.GAMESTATE.death
		if $headDown.is_active():
				$headDown.stop_all()
		$AnimatedSprite.play("idle"+String(spriteColor))
		$audio/hit.play()
		$audio/death.play()
		$"../CanvasLayer/flash".emitting = true
		if saveLoad.loadData().highScore <= globe.score:
			saveLoad.saveData()
		globe.score=0
		
	pass
	
func scoreCount():
	globe.score+=1
	$audio/score.play()
	pass



func _on_bird_area_entered_scoreCount(area):
	scoreCount()
	pass 
